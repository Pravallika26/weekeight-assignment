package com.hcl.mapping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.mapping.entity.Car;
@Repository
public interface CarRepository extends JpaRepository<Car, Integer>{

}
