package com.hcl.mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.mapping.entity.Car;
import com.hcl.mapping.entity.Person;
import com.hcl.mapping.repository.CarRepository;
import com.hcl.mapping.repository.PersonRepository;

@SpringBootApplication
public class OnetoOneApplication implements CommandLineRunner{

	@Autowired
	PersonRepository personrepo;
	
	@Autowired
	CarRepository carrepo;
	
	public static void main(String[] args) {
		SpringApplication.run(OnetoOneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		Person person = new Person();
		person.setName("Praval");person.setAge(21);
		//personrepo.save(person);
		
		Car car = new Car();
		car.setModel("ford");car.setColor("black");
		
		person.setCar(car);
		car.setPerson(person);
		//carrepo.save(car);
		personrepo.save(person);
		
		Car carDetails = carrepo.findById(2).orElse(null);
		System.out.println(carrepo);
	}

}
