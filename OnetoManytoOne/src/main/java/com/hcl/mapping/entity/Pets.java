package com.hcl.mapping.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
@Table(name="Pets_table")
public class Pets {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int petId;
	private String name;
	
	public Pets(String name) {
		super();
		this.name = name;
	}
	

}
