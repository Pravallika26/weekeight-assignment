package com.hcl.mapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.hcl.mapping.entity.Owner;
import com.hcl.mapping.entity.Pets;
import com.hcl.mapping.repository.OwnerRepository;
import com.hcl.mapping.repository.PetsRepository;

@SpringBootApplication
public class ManyToOneApplication implements CommandLineRunner {

	@Autowired
	private PetsRepository petsrepo;

	@Autowired
	private OwnerRepository ownerrepo;

	public static void main(String[] args) {
		SpringApplication.run(ManyToOneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub


		Owner owner = new Owner("rachel", "chennai");

		Pets p1 = new Pets("dog");
		Pets p2 = new Pets("cat");
		Pets p3 = new Pets("bird");

		owner.getPets().add(p1);
		owner.getPets().add(p2);
		owner.getPets().add(p3);

		ownerrepo.save(owner);

	}
}