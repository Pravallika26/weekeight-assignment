package com.hcl.mapping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.mapping.entity.Pets;

@Repository
public interface PetsRepository extends JpaRepository<Pets, Integer>{

}
