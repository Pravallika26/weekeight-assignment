package com.hcl.repository;

import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hcl.entity.Login;

@Repository
public class LoginRepo {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public int StoreRegistrationDetails(Login login) {
		try {
			return jdbcTemplate.update("INSERT INTO login values(?,?)", login.getEmail(), login.getPassword());

		} catch (Exception ex) {
			System.out.println("storing " + ex);
		}
		return 0;
	}

	public String checkLoginDetails(String email) {
		String password = (String) jdbcTemplate.queryForObject("SELECT password from login where email=?",
				new Object[] { email }, String.class);
		return password;

	}

}
