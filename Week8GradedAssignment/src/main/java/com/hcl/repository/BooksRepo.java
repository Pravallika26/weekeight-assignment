package com.hcl.repository;

import java.sql.ResultSet;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.hcl.entity.Books;

@Repository
public class BooksRepo {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<Books> getAllBooks() {
		try {
			return jdbcTemplate.query("SELECT * from books", new bookRowMapper());
		} catch (Exception ex) {
			System.err.println("Restoring books " + ex);
			return null;
		}
	}

	public int storeLikedBooks(Books books, String user) {
		try {
			return jdbcTemplate.update("INSERT INTO likedbooks values(?,?,?)", books.getBookId(), books.getTitle(),
					books.getGenre(), user);
		} catch (Exception ex) {
			System.err.println("Liked books " + ex);
			return 0;
		}
	}

	public int storeReadLaterBooks(Books books, String user) {
		try {
			return jdbcTemplate.update("INSERT INTO readlaterbooks values(?,?,?)", books.getBookId(), books.getTitle(),
					books.getGenre(), user);
		} catch (Exception ex) {
			System.err.println(" Readlater books " + ex);
			return 0;
		}
	}

	public List<Books> getAllLikedBooks(String user) {
		try {
			return jdbcTemplate.query("SELECT bookId,title,genre from likedbooks where user=?",
					new likedBookRowMapper(), user);
		} catch (Exception ex) {
			System.err.println(" GetlikedBooks " + ex);
			return null;
		}

	}

	public List<Books> getAllReadLaterBooks(String user) {
		try {
			return jdbcTemplate.query("SELECT bookId,title,genre from readlaterbooks where user=?",
					new readLaterBookRowMapper(), user);
		} catch (Exception ex) {
			System.err.println(" GetlikedBooks " + ex);
			return null;
		}

	}
}

class bookRowMapper implements RowMapper<Books> {

	@Override
	public Books mapRow(ResultSet result, int rowNum) throws SQLException {
		Books book = new Books();
		book.setBookId(result.getInt(1));
		book.setTitle(result.getString(2));
		book.setGenre(result.getString(3));

		return book;
	}

}

class likedBookRowMapper implements RowMapper<Books> {

	@Override
	public Books mapRow(ResultSet result, int rowNum) throws SQLException {
		Books liked = new Books();
		liked.setBookId(result.getInt(1));
		liked.setTitle(result.getString(2));
		liked.setGenre(result.getString(3));
		return liked;
	}

}

class readLaterBookRowMapper implements RowMapper<Books> {

	@Override
	public Books mapRow(ResultSet result, int rowNum) throws SQLException {
		Books later = new Books();
		later.setBookId(result.getInt(1));
		later.setTitle(result.getString(2));
		later.setGenre(result.getString(3));
		return later;
	}

}
