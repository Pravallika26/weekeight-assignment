package com.hcl.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.entity.Login;
import com.hcl.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	LoginService loginService;

	@RequestMapping(value = "welcome", method = RequestMethod.GET)
	public ModelAndView welcomepage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("login.jsp");
		return mv;
	}

	@RequestMapping(value = "Register", method = RequestMethod.GET)
	public ModelAndView Registrationpage() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("registration.jsp");
		return mv;
	}

	@RequestMapping(value = "Registration", method = RequestMethod.POST)
	public ModelAndView StoreRegistrationDetails(HttpServletRequest req) {
		ModelAndView mv = new ModelAndView();

		String email = req.getParameter("email");
		String pass = req.getParameter("pass");

		Login login = new Login();
		login.setEmail(email);
		login.setPassword(pass);

		String result = loginService.storeRegistrationInfo(login);
		if (result.equalsIgnoreCase("success")) {
			req.setAttribute("loginMessage", "You have registered ");
			mv.setViewName("login.jsp");

		} else {
			req.setAttribute("registermsg", "Registration failed...check again");
			mv.setViewName("index.jsp");
		}
		return mv;
	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView checkLoginDetails(HttpServletRequest req, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		
		String email = req.getParameter("email");
		String password = req.getParameter("pass");
		String user = email.substring(0, email.indexOf('@'));
		
		Login log = new Login();
		log.setEmail(email);
		log.setPassword(password);

		String result = loginService.checkLoginInfo(email);
		if (result.equalsIgnoreCase(password)) {
			session.setAttribute("user", user);
			mv.setViewName("dashboard.jsp");
		} else {
			req.setAttribute("flog", "Incorrect details...check");
			mv.setViewName("index.jsp");
		}

		return mv;

	}

}
