package com.hcl.controller;

import java.util.Iterator;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.hcl.entity.Books;
import com.hcl.service.BooksService;

@Controller
public class BookController {
	@Autowired
	BooksService booksService;
	List<Books> list;

	@RequestMapping(value = "display", method = RequestMethod.GET)
	public ModelAndView displayAllBooks(HttpServletRequest req, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		list = booksService.fetchAllBooks();
		session.setAttribute("obj", list);
		mv.setViewName("display.jsp");
		return mv;

	}

	@RequestMapping(value = "Dashboard", method = RequestMethod.GET)
	public ModelAndView displayBooksForUser(HttpSession session) {
		ModelAndView mv = new ModelAndView();
		list = booksService.fetchAllBooks();
		session.setAttribute("obj1", list);
		mv.setViewName("Welcome.jsp");
		return mv;

	}

	@RequestMapping(value = "like", method = RequestMethod.POST)
	public ModelAndView listOfBooks(HttpServletRequest req, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		if (req.getParameter("Like") != null) {
			int id = Integer.parseInt(req.getParameter("id"));
			Iterator<Books> iter = list.iterator();
			while (iter.hasNext()) {
				Books liked = iter.next();
				if (id == liked.getBookId()) {
					Object user = session.getAttribute("user");
					String result = booksService.storeLikedBooksInfo(liked, user.toString());
					mv.setViewName("Welcome.jsp");

				}

			}
		}
		return mv;
	}

	@RequestMapping(value = "readlater", method = RequestMethod.POST)
	public ModelAndView listOfReadLaterBooks(HttpServletRequest req, HttpSession session) {
		ModelAndView mv = new ModelAndView();
		if (req.getParameter("readlater") != null) {
			int id = Integer.parseInt(req.getParameter("id"));
			Iterator<Books> it = list.iterator();
			while (it.hasNext()) {
				Books later = it.next();
				if (id == later.getBookId()) {
					Object user = session.getAttribute("user");
					String result = booksService.storeReadLaterBooksInfo(later, user.toString());
					mv.setViewName("Welcome.jsp");

				}

			}
		}
		return mv;
	}

	@RequestMapping(value = "likedbooks", method = RequestMethod.GET)
	public ModelAndView displayLikedBooksForUser(HttpSession session) {
		ModelAndView mv = new ModelAndView();
		Object user = session.getAttribute("user");
		list = booksService.fetchAllLikedBooks(user.toString());
		session.setAttribute("obj2", list);
		System.out.println(list);
		mv.setViewName("likedbooks.jsp");
		return mv;

	}

	@RequestMapping(value = "readlaterbooks", method = RequestMethod.GET)
	public ModelAndView displayReadLaterBooksForUser(HttpSession session) {
		ModelAndView mv = new ModelAndView();
		Object user = session.getAttribute("user");
		list = booksService.fetchAllReadLikedBooks(user.toString());
		session.setAttribute("obj3", list);
		System.out.println(list);
		mv.setViewName("readlater.jsp");
		return mv;

	}

	@RequestMapping(value = "logout")
	public ModelAndView logout() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("logout.jsp");
		return mv;
	}

}