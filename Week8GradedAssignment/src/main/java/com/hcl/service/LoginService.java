package com.hcl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.entity.Login;
import com.hcl.repository.LoginRepo;

@Service
public class LoginService {
	@Autowired
	LoginRepo loginrepo;

	public String storeRegistrationInfo(Login login) {
		if (loginrepo.StoreRegistrationDetails(login) > 0) {
			System.out.println("service");
			return "Logged in";

		} else {
			return "Can't login";
		}
	}

	public String checkLoginInfo(String email) {
		return loginrepo.checkLoginDetails(email);

	}

}