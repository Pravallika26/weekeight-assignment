package com.hcl.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.entity.Books;
import com.hcl.repository.BooksRepo;

@Service
public class BooksService {
	@Autowired
	BooksRepo booksrepo;

	public List<Books> fetchAllBooks() {
		return booksrepo.getAllBooks();
	}

	public String storeLikedBooksInfo(Books book, String user) {
		if (booksrepo.storeLikedBooks(book, user) > 0) {
			return "Likedbooks stored";
		} else {
			return "Can't store";
		}
	}

	public String storeReadLaterBooksInfo(Books book, String user) {
		if (booksrepo.storeReadLaterBooks(book, user) > 0) {
			return "ReadLater books stored";
		} else {
			return "Can't store";
		}
	}

	public List<Books> fetchAllLikedBooks(String user) {
		return booksrepo.getAllLikedBooks(user);
	}

	public List<Books> fetchAllReadLikedBooks(String user) {
		return booksrepo.getAllReadLaterBooks(user);
	}

}
