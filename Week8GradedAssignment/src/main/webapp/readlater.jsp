<%@page import="com.hcl.entity.Books"%>
<%@page import="java.util.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<h4 align="center">ReadLater Books</h4>
<a href="Welcome.jsp">Home Page</a>
<body> 
<div align="center">
<table border="1">
	<tr>
			<th>BOOKID</th>
			<th>TITLE</th>
			<th>GENRE</th>
					
	</tr>
<% 

	Object user=session.getAttribute("user");
	if(user!=null){
	out.println("WELCOME "+user);
	}
	Object obj = session.getAttribute("obj3");
	List<Books> list = (List<Books>)obj;
	Iterator<Books> iter = list.iterator();
	while(iter.hasNext()){
		Books book  = iter.next();
		%>
		<tr>
			<td><%=book.getBookId() %></td>
			<td><%=book.getTitle() %></td>
			<td><%=book.getGenre() %></td>
			
			
		</tr>
		<% 
	}
%>
</table>
</div>
</body>
</html>