create database week8_book;
use week8_book;

create table Login(email varchar(70) primary key, password varchar(20));
create table LikedBooks(bookId int,title varchar(40),genre varchar(40));
create table ReadLaterBooks(bookId int,title varchar(40),genre varchar(40));

create table books(id int primary key ,title varchar(30),genre varchar(20));

 insert into books values(1,"Harry Potter","Fiction");

 insert into books values(2,"The Incredible Hulk","Comic");

insert into books values(3,"The women in white","Mystery");

insert into books values(4,"APJ Abdul Kalam","Autobiography");

insert into books values(5,"Steve Jobs","Biography");

 insert into books values(6,"Apples never fall","Thriller");

 insert into books values(7,"The mighty avengers","Comic");

 insert into books values(8,"Cruel Shoes","Humor");
 
 insert into books values(9,"Dracula","Fantasy");
 
 insert into books values(10,"The Secret Garden","Fantasy");

 select * from books;