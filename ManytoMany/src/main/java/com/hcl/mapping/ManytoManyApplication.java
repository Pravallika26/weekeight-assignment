package com.hcl.mapping;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.mapping.entity.Flight;
import com.hcl.mapping.entity.Traveler;
import com.hcl.mapping.repository.FlightRepository;
import com.hcl.mapping.repository.TravelerRepository;

@SpringBootApplication
public class ManytoManyApplication implements CommandLineRunner {

	@Autowired
	FlightRepository flightrepo;

	@Autowired
	TravelerRepository travelerrepo;

	public static void main(String[] args) {
		SpringApplication.run(ManytoManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		 
//		Flight flight = new Flight();
//		flight.setName("IndiGo");flight.setCity("delhi");
//		
//		Traveler t1 = new Traveler();
//		t1.setName("Praval");t1.setAge(21);	
//		Traveler t2 = new Traveler();
//		t2.setName("thor");t2.setAge(30);
//		Traveler t3 = new Traveler();
//		t3.setName("tony");t3.setAge(25);
//		
//		Set<Traveler> flight1 = new HashSet<Traveler>();
//		flight1.add(t1);
//		flight1.add(t3);
//		flight1.add(t1);
//		
//	flight.setTravelers(flight1);
//		flightrepo.save(flight);
		
		
       Flight flightAgain = new Flight();
       flightAgain.setName("AirIndia");flightAgain.setCity("bangalore");
       
		Traveler t4 = new Traveler();
		t4.setName("arya");t4.setAge(21);	
		Traveler t5 = new Traveler();
		t5.setName("sansa");t5.setAge(24);
		Traveler t6 = new Traveler();
		t6.setName("john");t6.setAge(25);
		
		Set<Traveler> flight2 = new HashSet<Traveler>();
		flight2.add(t4);
		flight2.add(t5);
		flight2.add(t6);
		
	flightAgain.setTravelers(flight2);
		flightrepo.save(flightAgain);
		
	}

}
