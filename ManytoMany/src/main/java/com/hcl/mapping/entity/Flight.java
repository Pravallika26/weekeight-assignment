package com.hcl.mapping.entity;

//import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
//import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter
@Getter
@NoArgsConstructor
@ToString
public class Flight {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int id;	
	private String name;
	@Column(name="destination")
	private String city;

	
	@ManyToMany(cascade = CascadeType.ALL , fetch=FetchType.EAGER)
	@JoinTable(name="airlines", 
	 joinColumns = {@JoinColumn(name="flight_id")},
	 inverseJoinColumns = {@JoinColumn(name="traveler_id")})
	private Set<Traveler> travelers = new HashSet<>();
	
	
}
