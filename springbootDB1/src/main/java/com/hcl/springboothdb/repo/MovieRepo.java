package com.hcl.springboothdb.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.springboothdb.entity.Movie;

public interface MovieRepo extends JpaRepository<Movie, Integer>{


}
