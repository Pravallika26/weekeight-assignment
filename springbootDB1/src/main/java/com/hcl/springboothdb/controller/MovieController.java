package com.hcl.springboothdb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springboothdb.entity.Movie;
import com.hcl.springboothdb.service.MovieService;

@RestController
@RequestMapping("/v1/movie")
public class MovieController {
	
	@Autowired
	MovieService service;
	
	@PostMapping(value = "/add",consumes = "application/json",produces = "application/json")
	public Movie add(@RequestBody  Movie movie) {		
		return service.addMovie(movie);	
	}
	
	@GetMapping("/get")
	public List<Movie> getAll(){
		return service.getMovie();
	}
	
	@GetMapping("/getbyid/{id}")
	public Movie getById(@PathVariable int id) {
//		boolean isMovieExist = service.isMovieExist(id);
//		if(isMovieExist) {
//			Movie movie = service.getMovieById(id);
			return service.getMovieById(id);
//		}else {
//			throw new MovieNotFoundException();
//		}
		
	}
	
	@PutMapping(value = "/update",consumes = "application/json",produces = "application/json")
	public Movie update(@RequestBody  Movie movie) {		
		return service.update(movie);	
	}
	
	@DeleteMapping("/delete/{id}")
	public String delete(@PathVariable int id) {
		return service.delete(id);
	}
	
	@DeleteMapping("/remove")
	public String deleted() {
		return service.deleteall();
	}

}
