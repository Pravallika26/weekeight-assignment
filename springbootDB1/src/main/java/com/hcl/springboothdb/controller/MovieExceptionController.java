package com.hcl.springboothdb.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.hcl.springboothdb.exception.MovieNotFoundException;

public class MovieExceptionController {
	
	@ExceptionHandler(value=MovieNotFoundException.class)
	public ResponseEntity<Object> exception(MovieNotFoundException exception){
		return new ResponseEntity<>("Movie not found",HttpStatus.NOT_FOUND);
	}

}
