package com.hcl.springboothdb.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Movie_info")
public class Movie {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getLanguage() {
		return language;
	}
	public double getRating() {
		return rating;
	}
	@Column(name="lang")
	private String language;
	private double rating;

}
