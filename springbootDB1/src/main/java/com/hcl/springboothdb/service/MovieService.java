package com.hcl.springboothdb.service;

import java.util.List;

import com.hcl.springboothdb.entity.Movie;

public interface MovieService {
	
	public Movie addMovie(Movie movie);
	
	public List<Movie> getMovie();
	
	public Movie getMovieById(int id);
	
	public String delete(int id);
	
	public Movie update(Movie movie);
	
	public String deleteall();
	
}
