package com.hcl.springboothdb.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springboothdb.entity.Movie;
import com.hcl.springboothdb.repo.MovieRepo;

@Service 
public class MovieServiceImp  implements MovieService{
	
	@Autowired
	MovieRepo repo;
	
	public Movie addMovie(Movie movie) {
		return repo.save(movie);
		
	}
	
	public List<Movie> getMovie(){
		return repo.findAll();
	}
	
	public Movie getMovieById(int id){
		return repo.findById(id).orElse(null);
	}
	
	public String delete(int id) {
		repo.deleteById(id);
		return "Movie deleted";
	}
	
	public Movie update(Movie movie) {
		return repo.save(movie);
	}

	public boolean isMovieExist(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String deleteall() {
		// TODO Auto-generated method stub
		repo.deleteAll();
		return "deleted all";
	}	

}
