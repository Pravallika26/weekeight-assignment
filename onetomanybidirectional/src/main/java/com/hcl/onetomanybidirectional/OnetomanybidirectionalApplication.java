package com.hcl.onetomanybidirectional;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.onetomanybidirectional.entity.Owner;
import com.hcl.onetomanybidirectional.entity.Pets;
import com.hcl.onetomanybidirectional.repository.OwnerRepository;
import com.hcl.onetomanybidirectional.repository.PetsRepository;

@SpringBootApplication
public class OnetomanybidirectionalApplication implements CommandLineRunner{
	@Autowired
	private PetsRepository petsrepo;
	
	@Autowired
	private OwnerRepository ownerrepo;

	public static void main(String[] args) {
		SpringApplication.run(OnetomanybidirectionalApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		Owner owner = new Owner("rachel","chennai"); 	 
		 
		  Pets p1 = new Pets("dog");
		  Pets p2 = new Pets("cat");
		  Pets p3 = new Pets("bird"); 
		  
		  owner.getPets().add(p1);
		  owner.getPets().add(p2);
		  owner.getPets().add(p3);
		  
		  p1.setOwner(owner);
		  p2.setOwner(owner);
		  p3.setOwner(owner);
		  
		  ownerrepo.save(owner);
		  
//		  Pets pet = petsrepo.findById(3).orElse(null);
//		  System.out.println(pet);
	}

}
