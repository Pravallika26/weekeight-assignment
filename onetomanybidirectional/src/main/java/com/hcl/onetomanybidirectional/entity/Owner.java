package com.hcl.onetomanybidirectional.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;

//import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Entity
@Setter
@Getter
@NoArgsConstructor
//@Data
@ToString
@Table(name="Owner_table")
public class Owner {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int OwnerId;
	private String name;
	private String city;
	
	public Owner(String name, String city) {
		super();
		this.name = name;
		this.city = city;
	}

	//@JoinColumn(name="op_id",referencedColumnName = "OwnerId")
	@OneToMany(mappedBy = "owner", cascade=CascadeType.ALL)
	private List<Pets> pets = new ArrayList<>(); 
	
	
	
}

