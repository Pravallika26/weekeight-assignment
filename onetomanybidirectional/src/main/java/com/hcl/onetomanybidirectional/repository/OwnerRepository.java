package com.hcl.onetomanybidirectional.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.onetomanybidirectional.entity.Owner;

public interface OwnerRepository extends JpaRepository<Owner, Integer>{

}
