package com.hcl.onetomanybidirectional.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.onetomanybidirectional.entity.Pets;

public interface PetsRepository extends JpaRepository<Pets, Integer>{

}
